package ch.bfh.gnagj1.Base;

public class Person implements IPerson{

	protected String aName;
	protected String aForename;
	
	public Person( String aForename,String aName)
	{
		this.aForename = aForename;
		this.aName = aName;
	}
	
	@Override
	public String getFirstname() {
		// TODO Auto-generated method stub
		return aForename;
	}

	@Override
	public String getLastname() {
		// TODO Auto-generated method stub
		return aName;
	}
	
	
}
