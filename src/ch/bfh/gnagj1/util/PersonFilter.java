package ch.bfh.gnagj1.util;
import java.util.ArrayList;

import ch.bfh.gnagj1.Base.IPerson;
import ch.bfh.gnagj1.Base.IPersonFilter;

public class PersonFilter implements IPersonFilter {

	IPersonFilter specFilter;
	
	
	public PersonFilter(IPersonFilter filter)
	{
		this.specFilter = filter == null ? this : filter; 	
	}
	
	/**
	 * Partially shows the given list by using the filter callback object.
	 * 
	 * @param list
	 *            The input list.
	 */
	public void filter(ArrayList<IPerson> list) {
		for (IPerson p : list)
			if (specFilter.filter(p))
				// show the object only if the filter callback returns true
				System.out.println(p);
	}

	@Override
	public boolean filter(IPerson person) {
		// TODO Auto-generated method stub
		return true;
	}
	
	
}
