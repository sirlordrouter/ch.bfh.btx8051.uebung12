package ch.bfh.gnagj1.util;
import ch.bfh.gnagj1.Base.*;

/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Filters Persons based on a given Key by Forename.</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 26.11.2012
 */

public class NameFilter implements IPersonFilter {

	public enum FilterMethod { Forename, Name };
	private FilterMethod method;
	private String filterKey;
	
	
	public NameFilter(FilterMethod method, String key)
	{
		this.method = method; 
		this.filterKey = key; 
	}
	
	@Override
	public boolean filter(IPerson person) {
		
		if (method.equals(FilterMethod.Forename)) {
			return person.getFirstname().startsWith(filterKey) ? true : false;
		} else {
			return person.getLastname().startsWith(filterKey) ? true : false;
		}
		 
	}

}
