/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Contains Util-CLasses. In this exercise there are only two FilterClasses.</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 27.11.2012
 */
package ch.bfh.gnagj1.util;