package ch.bfh.gnagj1.test;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;

import ch.bfh.gnagj1.Base.*;
import ch.bfh.gnagj1.uebung12.*;
import ch.bfh.gnagj1.util.NameFilter;
import ch.bfh.gnagj1.util.PersonFilter;

public class InterfaceTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String filterstring = "";
		String filtermethod = "";
		ArrayList<IPerson> list = new ArrayList<IPerson>();

		list.add(new Doctor("Petra", "Meier", "Dr. med.", "071 717 71 17"));
		list.add(new Doctor("Hans", "Meier", "Prof Dr. med.", "033 333 33 33"));
		list.add(new Patient("Hans", "Muster",
				new GregorianCalendar(1953, 5, 2), new Address("Hintergasse 5",
						"3012", "Bern")));
		list.add(new Patient("Hans", "Meier",
				new GregorianCalendar(1972, 1, 1), new Address(
						"Vordergasse 15", "3012", "Bern")));
		list.add(new Patient("Hans", "Moser",
				new GregorianCalendar(1988, 9, 24), new Address("Irgendwo 12",
						"2504", "Biel")));
		list.add(new Doctor("Georg F.", "Strahlemann", "Dr. med.",
				"099 837 98 92"));
		list.add(new Doctor("Susy.", "Moser", "PD Dr. med.", "099 929 99 99"));
		list.add(new Patient("Johnny", "Black", new GregorianCalendar(1967, 4,
				5), new Address("Schnellstrasse 99", "3012", "Bern")));
		list.add(new Patient("Peter", "White",
				new GregorianCalendar(1959, 5, 4), new Address("Hinterweg",
						"2501", "Biel")));
		list.add(new Patient("Sam", "Brown",
				new GregorianCalendar(1954, 3, 15), new Address("Vorderweg 8",
						"2550", "Nidau")));
		list.add(new Patient("Sally", "Field", new GregorianCalendar(1971, 12,
				11), new Address("Baumallee 2", "3012", "Bern")));
		list.add(new Patient("Schorsch", "Habakuk", new GregorianCalendar(1935,
				12, 25), new Address("Uferweg 22", "2555", "Bruegg")));

		Scanner in = new Scanner(System.in);

		while (true) {

			System.out
					.println("Geben Sie einen beliebeigen Buchstabe ein, nach welchem die Personen gefiltert werden sollen.");
			filterstring = in.next("[A-Z]");

			System.out
					.println("Select Filtermethod. N: Lastname, F: Forename.");
			filtermethod = in.next("[N,F]");

			IPersonFilter filter = null;

			switch (filtermethod) {
			case "N":
				filter = new NameFilter(NameFilter.FilterMethod.Name,
						filterstring);
				break;
			case "F":
				filter = new NameFilter(NameFilter.FilterMethod.Forename,
						filterstring);
			default:
				break;
			}

			PersonFilter personFilter = new PersonFilter(filter);

			System.out.println("Persons with a lastname starting with '"
					+ filterstring + "':");
			System.out
					.println("--------------------------------------------------");
			personFilter.filter(list);
		}

	}

}
