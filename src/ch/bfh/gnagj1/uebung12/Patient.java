package ch.bfh.gnagj1.uebung12;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import ch.bfh.gnagj1.Base.Person;


/**
 * Berner Fachhochschule</br> Medizininformatik BSc</br> Modul 8051-HS2012</br>
 * 
 * <p>
 * Describes a Patient.
 * </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 26.11.2012
 */

public class Patient extends Person {

	private GregorianCalendar aBirthdate;
	private Address aAddress;

	public Patient( String aForename,String aName,
			GregorianCalendar aBirthdate, Address aAddress) {
	    super(aForename,aName);
		this.aBirthdate = aBirthdate;
		this.aAddress = aAddress;
	}

	public String getBirthdate() {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
		return formatter.format(aBirthdate.getTime());
	}

	public Address getAddress() {
		return aAddress;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return aName + ", "+ aForename +", "+ getBirthdate() + 
				", ("+ aAddress.getStreet() + ", " + aAddress.getZip() + " " + aAddress.getCity() + ")";
	}
	
	

}
