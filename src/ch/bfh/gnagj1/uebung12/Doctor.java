package ch.bfh.gnagj1.uebung12;
import ch.bfh.gnagj1.Base.Person;

/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Descripes a Person who is Doctor.</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 26.11.2012
 */

public class Doctor extends Person{

	private String aTitle;
	private String aPhoneNumber;
	
	public Doctor(String aForename, String aName, String aTitle, String aPhoneNumber)
	{
		super(aForename, aName);
		this.aTitle = aTitle;
		this.aPhoneNumber = aPhoneNumber; 
	}
	
	public String getTitle()
	{
		return aTitle;
	}
	
	public String getPhone()
	{
		return aPhoneNumber; 
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return aName + ", "+ aForename +", "+ aTitle +", "+ aPhoneNumber;
	}

}
