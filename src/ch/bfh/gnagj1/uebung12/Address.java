package ch.bfh.gnagj1.uebung12;
/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Describes an Address-Object.</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 26.11.2012
 */

public class Address
{
    String street;
    String zip;
    String city;
    
    public Address(String street, String zip, String city)
    {
	this.street = street;
	this.zip = zip;
	this.city = city;
    }
    
    public String getStreet()
    {
	return this.street;
    }
    
    public String getZip()
    {
	return this.zip;
    }
    
    public String getCity()
    {
	return this.city;
    }
    
    public String toString()
    {
	return String.format("%s, %s %s", this.street, this.zip, this.city);
    }
}
